import pytest
from django.urls import reverse

pytestmark = pytest.mark.django_db


def test_fake_user_agent_get_method(client, user_agent):
    expected_status_code = 200
    url = reverse('fake_user_agent')
    response = client.get(url)

    assert response.status_code == expected_status_code
    assert response.json()['fake_user_agent'] == user_agent.user_agent
