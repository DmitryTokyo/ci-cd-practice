from pytest_factoryboy import register

from user_agent.tests.factories import UserAgentFactory


register(UserAgentFactory)


