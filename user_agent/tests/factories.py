from factory.django import DjangoModelFactory
from factory.faker import Faker

from user_agent.models import UserAgent


class UserAgentFactory(DjangoModelFactory):
    class Meta:
        model = UserAgent

    user_agent = Faker('user_agent')
