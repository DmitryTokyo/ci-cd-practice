from django.http import JsonResponse
from django.views import View
from django.views.generic import TemplateView

from user_agent.models import UserAgent


class UserAgentView(TemplateView):
    template_name = 'user_agent.html'


class FakeUserAgentView(View):
    def get(self, request):
        random_fake_user_agents = UserAgent.objects.order_by('?').first()
        return JsonResponse(status=200, data={'fake_user_agent': random_fake_user_agents.user_agent})
