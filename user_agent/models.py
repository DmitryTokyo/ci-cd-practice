from django.db import models


class UserAgent(models.Model):
    user_agent = models.TextField('User Agent')
