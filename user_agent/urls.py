from django.urls import path

from user_agent import views

urlpatterns = [
    path('', views.UserAgentView.as_view(), name='user_agent'),
    path('fake-user-agent/', views.FakeUserAgentView.as_view(), name='fake_user_agent')
]
