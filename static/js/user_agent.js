const buttons = document.querySelectorAll('button');
const textBlock = document.querySelector('.content.text');

buttons.forEach(button => {
  button.addEventListener('click', async (e) => {
    e.preventDefault();
    const action = button.dataset.action;
    let useragent;
    let useragentElement = document.querySelector('p');
    if (!useragentElement) {
      useragentElement = document.createElement('p');
    }

    if (action === 'my_agent') {
      useragentElement.innerText = '';
      useragent = navigator.userAgent;
      useragentElement.innerText = useragent;
      textBlock.append(useragentElement);
    }
    const url = button.dataset.url;
    if (action === 'fake_agent') {
      const result = await makeRequestToUserAgentView(url);
      if (result.errors) {
        return;
      }
      if (result) {
        useragentElement.innerText = '';
        useragentElement.innerText = result.fake_user_agent;
        textBlock.append(useragentElement);
      }
    }


  })
})

const makeRequestToUserAgentView = async (url) => {
  const requestParams = {method: 'GET'};
  requestParams.headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  }
  console.log(url)
  const response = await fetch(url, requestParams);
  return await response.json();
}